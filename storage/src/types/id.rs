// -------------------------------------------------------------------------------------------------
// IdGen

#[derive(Debug,Default)]
pub struct IdGen(u8);

impl IdGen {

    pub fn generate<E: ?Sized>(&mut self) -> Id<E, u8> {
        let next = self.0.saturating_add(1);
        if self.0 == next {
            panic!("id space exhausted");
        }
        self.0 = next;

        Id::from(next)
    }
}

// -------------------------------------------------------------------------------------------------
// Id

/// Generic ID new-type
///
/// ## Generics
///
/// The `E` generic parameter links type to specific database *entity*. The `E` must/should be
/// 'marker' struct with no data and no implementation, provided only to appear in generic
/// [`Id`](self) and [`IdType`](crate::types::IdType) types.
///
/// The `T` generic parameter defines type of the id value. It need to be set to what-ever the
/// storage abstraction is using.
///
///
/// ## Blanked impls for `T`
///
/// The [`Id`](self) type provides implementations for many basic traits which allow us comparing
/// or ordering `Id` instance to value of `T`. But reverting the operation, ie comparing `T` to
/// `Id<E, T>` is not possible to implement in generic fashion.
///
/// It would require blanked implementation and that cannot be done due to orphan rule.
///
///
/// The implementation would be possible only if the first generic parameter `E` was covered by
/// *local* type. But we can't be sure of that because it *is blanket implementation*.
///
/// Of course, it is possible to add PartialEq impl on T for specific type alias like `Id<Entity,
/// u8>`. Which might be desirable or not, depending on how the id type alias is used.
///
/// ```ignore
/// impl<E: ?Sized, T> std::cmp::PartialEq<Id<E, T>> for T
/// where T: std::cmp::PartialEq
/// {
///     fn eq(&self, rhs: &Id<E, T>) -> bool {
///         self.eq(&rhs.data)
///     }
/// }
/// ```
///
#[cfg_attr(feature="serde", derive(serde::Serialize,serde::Deserialize))]
#[cfg_attr(feature="serde", serde(transparent))]
pub struct Id<E: ?Sized, T> {
    #[cfg_attr(feature="serde", serde(skip))]
    mark: std::marker::PhantomData<fn() -> E>,
    data: T,
}

impl<E: ?Sized, T> Id<E, T> {
    pub fn new(data: T) -> Self {
        Self {
            data,
            mark: std::marker::PhantomData,
        }
    }

    pub fn inner(&self) -> &T {
        &self.data
    }

    pub fn into_inner(self) -> T {
        self.data
    }
}

impl<E: ?Sized, T> crate::types::IdType<E> for Id<E, T>
where T: std::marker::Copy
{
    type Id = T;

    fn id(self) -> Self::Id {
        self.into_inner()
    }
}

impl<E: ?Sized, T> std::marker::Copy for Id<E, T>
where T: std::marker::Copy
{
}

impl<E: ?Sized, T> std::clone::Clone for Id<E, T>
where T: std::clone::Clone
{
    fn clone(&self) -> Self {
        // *self
        todo!()
    }
}

impl<E: ?Sized, T> std::hash::Hash for Id<E, T>
where T: std::hash::Hash
{
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.data.hash(state);
        self.mark.hash(state);
    }
}

impl<E: ?Sized, T> std::fmt::Debug for Id<E, T>
where T: std::fmt::Debug
{
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
       f.debug_tuple("Id")
           .field(&self.data)
           .finish()
    }
}

impl<E: ?Sized, T> std::fmt::Display for Id<E, T>
where T: std::fmt::Display
{
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.data)
    }
}

impl<E: ?Sized, T> std::cmp::Eq for Id<E, T>
where T: std::cmp::Eq,
{
}

impl<E: ?Sized, T> std::cmp::PartialEq<Self> for Id<E, T>
where T: std::cmp::PartialEq
{
    fn eq(&self, rhs: &Self) -> bool {
        self.data.eq(&rhs.data)
    }
}

impl<E: ?Sized, T> std::cmp::PartialEq<T> for Id<E, T>
where T: std::cmp::PartialEq
{
    fn eq(&self, rhs: &T) -> bool {
        self.data.eq(rhs)
    }
}

impl <E: ?Sized, T> std::cmp::Ord for Id<E, T>
where T: std::cmp::Ord,
{
    fn cmp(&self, other: &Id<E, T>) -> std::cmp::Ordering {
        self.data.cmp(&other.data)
    }
}

impl <E: ?Sized, T> std::cmp::PartialOrd<Self> for Id<E, T>
where T: std::cmp::PartialOrd
{
    fn partial_cmp(&self, rhs: &Self) -> Option<std::cmp::Ordering> {
        self.data.partial_cmp(&rhs.data)
    }
}

impl<E: ?Sized, T> std::convert::AsRef<T> for Id<E, T> {
    fn as_ref(&self) -> &T {
        &self.data
    }
}

impl<E: ?Sized, T> std::str::FromStr for Id<E, T>
where T: std::str::FromStr
{
    type Err = ParseIdError;

    fn from_str(text: &str) -> Result<Id<E, T>, ParseIdError> {
        match text.parse::<T>() {
            Ok(val) => Ok(Self::from(val)),
            Err(_)  => Err(ParseIdError(text.into())),
        }
    }
}

impl<E: ?Sized, T> std::convert::From<T> for Id<E, T> {
    fn from(data: T) -> Self {
        Self::new(data)
    }
}

// -------------------------------------------------------------------------------------------------
// ParseIdError

#[derive(Clone,Debug)]
pub struct ParseIdError(String);

impl std::error::Error for ParseIdError {
}

impl std::fmt::Display for ParseIdError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "cannot parse id '{}'", self.0)
    }
}
