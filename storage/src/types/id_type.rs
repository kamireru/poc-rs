//! ID type marker
//!
//! Trait and related types for marking new-types as database ID and then using them inside
//! storage data types. The idea is to have storage 'row' data-type that can **both** have and not
//! have an ID.
//!
//! See blog post about the idea here: [Some useful types for database-using Rust web apps](
//! https://boinkor.net/2024/04/some-useful-types-for-database-using-rust-web-apps/)
//!
//! ## Notes
//!
//! TODO: We should see how this pattern plays with generic `Id<E>` new-type for representing
//! multiple different ids. We may/may not be able to combine them
//!
//! ```ignore
//!
//! // this shoudld work fine as used in the article
//! struct Hoge<I: IdType<HogeId>> {
//! }
//!
//! // how this work, we do not know ???
//! struct Hoge;
//!
//! type HogeId = Id<Hoge>;
//!
//! struct HogeRow<I: IdType<Hoge>> {
//! }
//! ```
//!

// -------------------------------------------------------------------------------------------------
// IdType

/// Trait marks storage ID types
pub trait IdType<E: ?Sized>: Copy {
    type Id;

    /// Return the inner id
    fn id(self) -> Self::Id;
}

// -------------------------------------------------------------------------------------------------
// NoId

/// Storage ID that does not have value
///
/// The struct is used as placeholder value in storage data types to represent that ID that should
/// be there is not there yet. This allows us to have same data-type both with/out id:
///
/// - `Hoge<HogeId>` for variant from storage
/// - `Hoge<NoId>` for variant that has not been stored (and does not have id yet)
///
#[derive(Clone,Copy,Debug,Default,PartialEq,Eq)]
#[cfg_attr(feature="serde", derive(serde::Serialize))]
pub struct NoId;

impl<E> IdType<E> for NoId {
    type Id = std::convert::Infallible;

    fn id(self) -> Self::Id {
        unreachable!("you must not to access non-ID");
    }
}

#[cfg(feature="serde")]
impl<'de> serde::de::Deserialize<'de> for NoId {
    fn deserialize<D>(_deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::de::Deserializer<'de>,
    {
        Ok(NoId)
    }
}

