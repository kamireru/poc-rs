use std::sync::Arc;
use std::time::Duration;

use tokio::sync::Mutex;
use tokio::sync::MutexGuard;

use crate::model::NoId;
use crate::model::Refie;
use crate::model::RefieId;
use crate::model::RefieBuilder;

// -------------------------------------------------------------------------------------------------
// Storage

#[derive(Clone,Debug,Default)]
pub struct Storage {
    // lock delay multiplied by number of items in the storage. it is used to simulate long-er
    // running operations so that HTMX features regarding indicators can be showcased
    wait: Duration,

    data: Arc<Mutex<StoreHouse>>
}

impl Storage {

    pub fn per_item_wait(mut self, wait: Duration) -> Self {
        self.wait = wait;
        self
    }

    pub async fn lock(&self) -> StorageGuard {
        let guard = StorageGuard(self.data.lock().await);

        // simulate longer-running operation
        let wait = self.wait * guard.len() as u32;
        tokio::time::sleep(wait).await;

        guard
    }
}

impl<T: Into<StoreHouse>> std::convert::From<T> for Storage {
    fn from(source: T) -> Self {
        Self {
            wait: Duration::from_millis(0),
            data: Arc::new(Mutex::new(source.into())),
        }
    }
}

impl std::convert::From<Vec<Refie<NoId>>> for Storage {
    fn from(list: Vec<Refie<NoId>>) -> Self {
        Self::from(StoreHouse::from_iter(list))
    }
}

impl<T: Into<Refie<NoId>>> std::iter::FromIterator<T> for Storage {
    fn from_iter<I: IntoIterator<Item=T>>(iter: I) -> Self {
        let iter = iter.into_iter().map(Into::into);
        Self::from(StoreHouse::from_iter(iter))
    }
}

// -------------------------------------------------------------------------------------------------
// StorageGuard

pub struct StorageGuard<'s>(MutexGuard<'s, StoreHouse>);

impl<'s> StorageGuard<'s> {

    pub fn insert(&mut self, refie: Refie<NoId>) -> &Refie<RefieId> {
        let builder = RefieBuilder::from(refie)
            .id(self.0.next_id());

        self.0.data.push(builder.into());
        self.0.data.last().unwrap()
    }

    pub fn update(&mut self, mut refie: Refie<RefieId>) -> Result<&Refie<RefieId>, StoreError> {
        let inner = self.0.data
            .iter_mut()
            .find(|i| i.id() == refie.id());

        match inner {
            Some(i) => {
                std::mem::swap(i, &mut refie);
                Ok(i)
            },
            None    => {
                Err(StoreError::UpdateAfterRemove(refie.id()))
            },
        }
    }

    pub fn remove(&mut self, id: RefieId) -> Option<Refie<RefieId>> {
        let index = self.0.data
            .iter()
            .enumerate()
            .find(|(_ ,refie)| id == refie.id())
            .map(|(index, _)| index);

        match index {
            Some(i) => Some(self.0.data.remove(i)),
            None    => None,
        }
    }

    pub fn get(&self, id: RefieId) -> Option<&Refie<RefieId>> {
        self.0.data
            .iter()
            .find(|refie| id == refie.id())
    }

    pub fn iter(&self) -> impl Iterator<Item=&Refie<RefieId>> {
        self.0.data.iter()
    }

    pub fn is_empty(&self) -> bool {
        self.0.data.is_empty()
    }

    pub fn len(&self) -> usize {
        self.0.data.len()
    }
}

// -------------------------------------------------------------------------------------------------
// StoreHouse

#[derive(Debug,Default)]
struct StoreHouse {
    pub idgn: u8,
    pub data: Vec<Refie<RefieId>>,
}

impl StoreHouse {

    pub fn next_id(&mut self) -> RefieId {
        let next = self.idgn.saturating_add(1);
        if self.idgn == next {
            panic!("id space exhausted");
        }
        self.idgn = next;

        RefieId::new(next)
    }
}

impl std::iter::FromIterator<Refie<NoId>> for StoreHouse {
    fn from_iter<T: IntoIterator<Item = Refie<NoId>>>(iter: T) -> Self {
        let mut store = Self::default();

        for refie in iter.into_iter() {
            let builder = RefieBuilder::from(refie)
                .id(store.next_id());

            store.data.push(builder.into());
        }

        store
    }
}

// -------------------------------------------------------------------------------------------------
// StoreError

#[derive(Clone,Debug)]
pub enum StoreError {
    UpdateAfterRemove(RefieId),
}

impl std::error::Error for StoreError {
}

impl std::fmt::Display for StoreError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Self::UpdateAfterRemove(id) =>
                write!(f, "{}: update after remove", id),
        }
    }
}
