use crate::model::time::MINUTES_PER_HOUR;
use crate::model::time::MINUTES_PER_DAY;

use crate::model::time::Time;
use crate::model::time::ParseTimeError;

// -------------------------------------------------------------------------------------------------
// Span

/// Representation of time span
#[derive(Copy,Clone,Debug,Default,PartialEq,Eq,PartialOrd,Ord,Hash)]
#[cfg_attr(feature="serde", derive(serde::Serialize,serde::Deserialize))]
#[cfg_attr(feature="serde", serde(transparent))]
pub struct Span(u32);

impl Span {

    /// [`Span`] of minimum length
    pub const ZERO: Self = Self(0);

    /// Minimum day of time
    pub const MIN: Span = Span(0);

    /// Maximum day of time
    pub const MAX: Span = Span(MINUTES_PER_DAY);

    /// [`Span`] corresponding to minute
    pub const MINUTE: Self = Self(1);

    /// [`Span`] corresponding to hour
    pub const HOUR: Self = Self(MINUTES_PER_HOUR);

    /// [`Span`] corresponding to day
    pub const DAY: Self = Self(MINUTES_PER_DAY);

    /// Create from hour, minute pair
    #[must_use]
    #[inline]
    pub const fn from_hm(hour: u8, minute: u8) -> Self {
        Self(60 * (hour as u32) + (minute as u32))
    }

    /// Create new [`Span`] from minute count
    #[must_use]
    #[inline]
    pub const fn from_m(mins: u32) -> Self {
        Self(mins)
    }

    #[must_use]
    #[inline]
    pub const fn as_mins(&self) -> u32 {
        self.0
    }
}

impl std::fmt::Display for Span {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}:{:02}", self.0 / 60,  self.0 % 60)
    }
}

impl std::ops::Add<Span> for Span {
    type Output = Span;

    fn add(self, other: Span) -> Self::Output {
        Self(self.0.saturating_add(other.0))
    }
}

impl std::ops::AddAssign<Span> for Span {
    fn add_assign(&mut self, other: Span) {
        self.0 = self.0.saturating_add(other.0)
    }
}

impl std::ops::Sub<Span> for Span {
    type Output = Span;

    fn sub(self, other: Span) -> Self::Output {
        Self(self.0.saturating_sub(other.0))
    }
}

impl std::ops::SubAssign<Span> for Span {
    fn sub_assign(&mut self, other: Span) {
        self.0 = self.0.saturating_sub(other.0)
    }
}

impl std::ops::Mul<u32> for Span {
    type Output = Span;

    fn mul(self, other: u32) -> Self::Output {
        Self(self.0.saturating_mul(other))
    }
}

impl std::ops::MulAssign<u32> for Span {
    fn mul_assign(&mut self, other: u32) {
        self.0 = self.0.saturating_mul(other)
    }
}

impl std::ops::Mul<Span> for u32 {
    type Output = Span;

    fn mul(self, other: Span) -> Self::Output {
        Span(other.0.saturating_mul(self))
    }
}

impl std::ops::Div<u32> for Span {
    type Output = Span;

    fn div(self, other: u32) -> Self::Output {
        Self(self.0.saturating_div(other))
    }
}

impl std::ops::DivAssign<u32> for Span {
    fn div_assign(&mut self, other: u32) {
        self.0 = self.0.saturating_div(other)
    }
}

impl std::convert::From<u32> for Span {
    fn from(data: u32) -> Self {
        Self(data)
    }
}

impl std::convert::From<u16> for Span {
    fn from(data: u16) -> Self {
        Self(data as u32)
    }
}

impl std::convert::From<u8> for Span {
    fn from(data: u8) -> Self {
        Self(data as u32)
    }
}

/// Convert [`Time`] into [`Span`] using notion that [`Span`] is duration from start of the day
/// until given [`Time`] instance.
impl std::convert::From<Time> for Span {
    fn from(time: Time) -> Self {
        Self(time.as_mins())
    }
}

impl std::str::FromStr for Span {
    type Err = ParseTimeError;

    fn from_str(text: &str) -> Result<Self, Self::Err> {
        text.parse::<Time>().map(Span::from)
    }
}
