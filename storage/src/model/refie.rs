// NOTE: If we leave id generation to external source (database), we would *never* need to
// create instance by hand. It would be created either by:
//
// - deserialization from serde
// - deserialization from database
//
// In our case, we are using in-memory storage with hand-rolled id sequence generation.
// Therefore we need a way to construct new instances explicitely.
// But it makes sense to have `new()` method instead of implemenenting conversion from inner
// type. Because it makes it more explicit that we allow *creation*, but not *conversion*.

use crate::types;
use crate::types::IdType;

use crate::entity;

use crate::model::Time;
use crate::model::Span;

// -------------------------------------------------------------------------------------------------
// RefieRow

/// Type alias for [`Refie`] struct with valid ID
pub type RefieRow = Refie<RefieId>;

// -------------------------------------------------------------------------------------------------
// RefieId

/// type alias for [`Refie`] database id
pub type RefieId = types::Id<entity::Refie, u8>;

// -------------------------------------------------------------------------------------------------
// Refie

/// Abstraction for 'refie'
///
/// The `refie` represent an entry in very simple time tracker with human readable label and time
/// range.
///
/// Why? Because we need structure that is bit more complex to really workout how the data flow
/// through application layers. From top of the head, few technical reasons:
///
/// - we want fields that need parsing/formatting
/// - we want fields that need validation
/// - we want multiple of them to test multi-error handling
///
/// And of course there are non-technical reasons:
///
/// - todo list has been done to death
/// - this may become prototype for real app
///
#[derive(Clone,Debug,PartialEq,Eq)]
#[cfg_attr(feature="serde", derive(serde::Serialize,serde::Deserialize))]
pub struct Refie<ID> {
    id: ID,
    from: Time,
    span: Span,
    label: String,
}

impl<ID: IdType<entity::Refie>> Refie<ID> {

    pub fn builder<S: Into<String>>(id: ID, label: S) -> RefieBuilder<ID> {
        RefieBuilder::new(id, label)
    }

    /// id
    pub fn id(&self) -> ID {
        self.id
    }

    /// time range start
    pub fn time_from(&self) -> Time {
        self.from
    }

    /// time range end
    pub fn time_upto(&self) -> Time {
        self.from + self.span
    }

    /// time range duration
    pub fn time_span(&self) -> Span {
        self.span
    }

    /// label
    pub fn label(&self) -> &str {
        &self.label
    }
}

impl<ID: IdType<entity::Refie>> std::convert::From<RefieBuilder<ID>> for Refie<ID> {
    fn from(builder: RefieBuilder<ID>) -> Self {
        builder.build()
    }
}

// -------------------------------------------------------------------------------------------------
// RefieBuilder

#[derive(Debug,Clone,PartialEq,Eq)]
pub struct RefieBuilder<ID: IdType<entity::Refie>>(Refie<ID>);

impl<ID: IdType<entity::Refie>> RefieBuilder<ID> {

    pub fn new<T: Into<String>>(id: ID, label: T) -> Self {
        Self(Refie {
            id,
            from: Time::ZERO,
            span: Span::ZERO,
            label: label.into(),
        })
    }

    /// Set `id` and change builder type
    pub fn id<NID: IdType<entity::Refie>>(self, id: NID) -> RefieBuilder<NID> {
        RefieBuilder(Refie {
            id,
            from: self.0.from,
            span: self.0.span,
            label: self.0.label,
        })
    }

    pub fn with_range(mut self, from: Time, upto: Time) -> Self {
        self.0.from = from;
        self.0.span = upto - from;
        self
    }

    pub fn with_span(mut self, from: Time, span: Span) -> Self {
        self.0.from = from;
        self.0.span = span;
        self
    }

    pub fn time_from(mut self, from: Time) -> Self {
        self.0.from = from;
        self
    }

    pub fn time_span(mut self, span: Span) -> Self {
        self.0.span = span;
        self
    }

    pub fn label<S: Into<String>>(mut self, label: S) -> Self {
        self.0.label = label.into();
        self
    }

    pub fn build(self) -> Refie<ID> {
        self.0
    }
}

impl<ID: IdType<entity::Refie>> std::convert::From<Refie<ID>> for RefieBuilder<ID> {
    fn from(refie: Refie<ID>) -> Self {
        Self(refie)
    }
}
