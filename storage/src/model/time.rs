use crate::model::span::Span;

// -------------------------------------------------------------------------------------------------
// constants

pub(crate) const MINUTES_PER_HOUR: u32 = 60;
pub(crate) const MINUTES_PER_DAY:  u32 = 24 * MINUTES_PER_HOUR;

// -------------------------------------------------------------------------------------------------
// Time

/// Representation of time offset
#[derive(Copy,Clone,Debug,Default,PartialEq,Eq,PartialOrd,Ord,Hash)]
#[cfg_attr(feature="serde", derive(serde::Serialize,serde::Deserialize))]
#[cfg_attr(feature="serde", serde(transparent))]
pub struct Time(u32);

impl Time {

    /// Minimum day of time
    pub const ZERO: Time = Time(0);

    /// Maximum day of time
    pub const DAY: Time = Time(MINUTES_PER_DAY);

    /// Minimum day of time
    pub const MIN: Time = Time(0);

    /// Maximum day of time
    pub const MAX: Time = Time(MINUTES_PER_DAY);

    /// Create from minute count
    #[must_use]
    #[inline]
    pub const fn from_m(mins: u32) -> Self {
        Self(mins)
    }

    /// Create from hour and minute pair
    #[must_use]
    #[inline]
    pub const fn from_hm(hour: u8, minute: u8) -> Self {
        Self(60 * (hour as u32) + (minute as u32))
    }


    /// Return hour component
    #[inline]
    pub fn hour(&self) -> u8 {
        (self.0 / 60) as u8
    }

    /// Return minute component
    #[inline]
    pub fn minute(&self) -> u8 {
        (self.0 % 60) as u8
    }

    /// Return number of mins in time of day
    #[inline]
    pub const fn as_mins(&self) -> u32 {
        self.0
    }
}

impl std::fmt::Display for Time {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{:02}:{:02}", self.hour(), self.minute())
    }
}

impl std::convert::From<u32> for Time {
    fn from(data: u32) -> Self {
        Self(data)
    }
}

impl std::convert::From<u16> for Time {
    fn from(data: u16) -> Self {
        Self(data as u32)
    }
}

impl std::convert::From<u8> for Time {
    fn from(data: u8) -> Self {
        Self(data as u32)
    }
}

impl std::convert::From<Span> for Time {
    fn from(span: Span) -> Self {
        Self(span.as_mins())
    }
}

impl std::str::FromStr for Time {
    type Err = ParseTimeError;

    fn from_str(text: &str) -> Result<Self, Self::Err> {
        if text.is_empty() {
            return Err(ParseTimeError::Empty);
        }

        Ok(match text.split_once(':') {
            Some((h, m)) => Time::from_hm(
                h.parse::<u8>().map_err(|_| ParseTimeError::InvalidHour)?,
                m.parse::<u8>().map_err(|_| ParseTimeError::InvalidMinute)?,
            ),
            None => Time(
                text.parse::<u32>().map_err(|_| ParseTimeError::InvalidMinute)?,
            ),
        })
    }
}

impl std::ops::Sub<Time> for Time {
    type Output = Span;

    fn sub(self, time: Time) -> Self::Output {
        Span::from_m(self.0.saturating_sub(time.0))
    }
}

impl std::ops::Sub<Span> for Time {
    type Output = Time;

    fn sub(self, span: Span) -> Self::Output {
        Self(self.0.saturating_sub(span.as_mins()))
    }
}

impl std::ops::Add<Span> for Time {
    type Output = Time;

    fn add(self, span: Span) -> Self::Output {
        Self(self.0.saturating_add(span.as_mins()))
    }
}

// -------------------------------------------------------------------------------------------------
// ParseTimeError

#[derive(Copy,Clone,Debug,PartialEq,Eq)]
pub enum ParseTimeError {
    Empty,
    InvalidHour,
    InvalidMinute,
}

impl std::error::Error for ParseTimeError {
}

impl std::fmt::Display for ParseTimeError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            Self::Empty         => "empty",
            Self::InvalidHour   => "invalid hour part",
            Self::InvalidMinute => "invalid minute part",
        })
    }
}
