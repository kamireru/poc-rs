//! # Entity Marker Types
//!
//! The [`module`](self) defines 'marker' types for annotating/marking generic types as being
//! relevant to specific database entities.
//!
//! The main use is for using with [`Id`](crate::types::Id`) and [`IdType`](crate::types::IdType`]
//! generics for defining zero-cost abstractions for various database ids.
//!

pub struct Refie;
