//! Database Types
//!
//! The [`module`](self) provides types that would normaly be part of low-level crate with
//! database utilities. Generic abstractions for building abstractions. Stuff that is used to build
//! storage crate, but is not specific to it.
//!
//! Since we do not have low-level utility crate, we just aggregate them in the [`module`](self)
//! and document why they are there. 
//!

mod id_type;
pub use id_type::IdType;
pub use id_type::NoId;

mod id;
pub use id::Id;
pub use id::ParseIdError;

