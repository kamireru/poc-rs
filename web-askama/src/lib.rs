mod index;
mod refie;

// -------------------------------------------------------------------------------------------------
// router()

/// Create application [`Router`]
pub fn routes() -> axum::Router<storage::Storage> {
    axum::Router::new()
        .nest("/",      index::routes())
        .nest("/refie", refie::routes())
}
