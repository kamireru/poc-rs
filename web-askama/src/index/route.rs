use axum::response::IntoResponse;

use super::html;

// -------------------------------------------------------------------------------------------------
// GET /

pub async fn index() -> axum::response::Response {
    html::Page::new("POC: HTMX + Askama").into_response()
}
