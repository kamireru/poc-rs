// -------------------------------------------------------------------------------------------------
// Page

#[derive(Debug)]
#[derive(askama::Template)]
#[template(path="index/page.html")]
pub struct Page<'a> {
    pub title: &'a str,
}

impl<'a> Page<'a> {

    pub fn new(title: &'a str) -> Self {
        Self { title }
    }
}
