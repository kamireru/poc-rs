use axum::Router;
use axum::routing::get;

use storage::Storage;

mod route;
mod html;

// -------------------------------------------------------------------------------------------------
// router()

/// Create application [`Router`]
pub fn routes() -> Router<Storage> {
    Router::new()
        .route("/", get(route::index))
}
