use axum::Router;
use axum::routing::get;

use storage::Storage;

mod route;
mod form;
mod html;
mod data;

// -------------------------------------------------------------------------------------------------
// router()

/// Create application [`Router`]
pub fn routes() -> Router<Storage> {
    Router::new()
        .route("/",          get(route::list).post(route::create))
        .route("/:id",      get(route::view).put(route::update).delete(route::remove))
        .route("/:id/edit", get(route::edit))
}
