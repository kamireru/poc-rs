use storage::model;

// -------------------------------------------------------------------------------------------------
// Refies

#[derive(Debug)]
#[derive(askama::Template)]
#[template(path="refie/list.html",escape="none")]
pub struct Refies {
    pub refies: Vec<RefieView>,
}

impl Refies {
    pub fn new(refies: Vec<RefieView>) -> Self {
        Self { refies }
    }

    pub fn len(&self) -> usize {
        self.refies.len()
    }
}

// -------------------------------------------------------------------------------------------------
// Refie

#[derive(Debug)]
#[derive(askama::Template)]
#[template(path="refie/view.html")]
pub struct RefieView {
    pub refie: model::RefieRow,
}

impl std::convert::From<model::RefieRow> for RefieView {
    fn from(refie: model::RefieRow) -> Self {
        Self { refie }
    }
}

#[derive(Debug)]
#[derive(askama::Template)]
#[template(path="refie/edit.html")]
pub struct RefieEdit {
    pub form_id: String,
    pub form_ref: String,

    pub refie: model::RefieRow,
}

impl std::convert::From<model::RefieRow> for RefieEdit {
    fn from(refie: model::RefieRow) -> Self {
        Self {
            form_id:  format!(r#"id="refie-form-{}""#, refie.id()),
            form_ref: format!(r#"form="refie-form-{}""#, refie.id()),
            refie,
        }
    }
}

// -------------------------------------------------------------------------------------------------
// ErrorSet

#[derive(Debug,Default)]
#[derive(askama::Template)]
#[template(path="refie/errors.html")]
pub struct ErrorSet {
    pub errors: Vec<String>,
}

impl<E: std::error::Error + 'static> std::convert::From<E> for ErrorSet {
    fn from(error: E) -> Self {
        use std::error::Error;

        let mut error: &(dyn Error + 'static) = &error as &(dyn Error);
        loop {
            error = match error.source() {
                Some(source) => source,
                None => return Self {
                    errors: vec![ format!("{}", error) ],
                },
            }
        }
    }
}
