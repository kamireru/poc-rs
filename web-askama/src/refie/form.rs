use super::data::Time;

// -------------------------------------------------------------------------------------------------
// RefieUpdate

#[derive(Debug,Clone,PartialEq,Eq,serde::Deserialize)]
pub struct RefieUpdate {
    pub from: Time,
    pub upto: Time,

    pub label: String,
}
