// NOTE: We do not really need another type and serde impls for it here. These could all be
// implemented on [`model::Time`] structs.
//
// But we do intentionally here, to show that the web backend layer can use helper/wrapper types
// for custom types used only at boundary between front/back-end.
//
// In this case, **storage** abstractions:
//
// - use simpler serialization
// - use different type for Time/Span
// - have lot of helper methods
//
// While **backend** abstractions:
//
// - use complex serialization
// - use single type for Time/Span as semantics is not important
// - implement only few methods as they are only wrappers
// - implement neccessary conversions
//

use storage::model;

// -------------------------------------------------------------------------------------------------
// Time

/// Representation of time offset (in minutes)
#[derive(Copy,Clone,Debug,Default,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub struct Time(u32);

impl Time {

    #[inline]
    pub fn hour(&self) -> u8 {
        (self.0 / 60) as u8
    }

    /// Return minute component
    #[inline]
    pub fn minute(&self) -> u8 {
        (self.0 % 60) as u8
    }
}

impl std::convert::From<Time> for model::Time {
    fn from(time: Time) -> Self {
        Self::from_m(time.0)
    }
}

impl std::convert::From<Time> for model::Span {
    fn from(time: Time) -> Self {
        Self::from_m(time.0)
    }
}

impl serde::Serialize for Time {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where S: serde::ser::Serializer,
    {
        serializer.serialize_str(&format!("{}", self.0))
    }
}

impl<'de> serde::Deserialize<'de> for Time {
    fn deserialize<D>(deser: D) -> Result<Self, D::Error>
    where
        D: serde::de::Deserializer<'de>,
    {
        deser.deserialize_any(TimeVisitor)
    }
}

#[derive(Clone,Copy)]
struct TimeVisitor;

impl<'de> serde::de::Visitor<'de> for TimeVisitor {
    type Value = Time;

    fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
        formatter.write_str("an u32 or string")
    }

    fn visit_str<E: serde::de::Error>(self, value: &str) -> Result<Self::Value, E> {
        if value.is_empty() {
            return Err(E::custom("empty string"));
        }

        let (h, m) = match value.split_once(':') {
            Some(pair) => pair,
            None       => ("", value),
        };

        let h = h.parse::<u8>()
            .map_err(|_| E::custom("invalid hour component"))
            .map(|v| v as u32)?;

        let m = m.parse::<u8>()
            .map_err(|_| E::custom("invalid minute component"))
            .map(|v| v as u32)?;

        Ok(Time(h * 60 + m))
    }

    fn visit_u8<E: serde::de::Error>(self, value: u8) -> Result<Self::Value, E> {
        Ok(Time(value as u32))
    }

    fn visit_u16<E: serde::de::Error>(self, value: u16) -> Result<Self::Value, E> {
        Ok(Time(value as u32))
    }

    fn visit_u32<E: serde::de::Error>(self, value: u32) -> Result<Self::Value, E> {
        Ok(Time(value))
    }
}
