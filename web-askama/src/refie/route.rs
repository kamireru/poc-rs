use axum::extract::State;
use axum::extract::Path;
use axum::extract::Form;
use axum::extract::rejection::FormRejection;
use axum::http::StatusCode;
use axum::response::IntoResponse;
use axum::response::Redirect;

use storage::Storage;
use storage::model;
use storage::model::Refie;
use storage::model::RefieId;

use super::html;
use super::form;

// -------------------------------------------------------------------------------------------------
// GET /refie

/// List [`Refie`]s in storage
pub async fn list(
    State(storage): State<Storage>,
) -> axum::response::Response
{
    let guard = storage.lock().await;
    let refies = guard.iter()
        .cloned()
        .map(html::RefieView::from)
        .collect::<Vec<_>>();

    html::Refies::new(refies)
        .into_response()
}

// -------------------------------------------------------------------------------------------------
// POST /refie

/// Create new [`Refie`]
pub async fn create(
    State(storage): State<Storage>,
    form: Result<Form<form::RefieUpdate>, FormRejection>,
) -> axum::response::Result<Redirect, (StatusCode, html::ErrorSet)>
{
    let form = match form {
        Ok(Form(data)) => data,
        Err(error)     => return Err((error.status(), html::ErrorSet::from(error))),
    };

    let refie = Refie::builder(model::NoId, form.label)
        .with_range(model::Time::from(form.from), model::Time::from(form.upto))
        .build();

    let mut guard = storage.lock().await;
    guard.insert(refie);

    Ok(Redirect::to("/refie"))
}

// -------------------------------------------------------------------------------------------------
// PUT /refie/:id

/// Update [`Refie`]s by `id`
pub async fn update(
    State(storage): State<Storage>,
    Path(id): Path<RefieId>,
    form: Result<Form<form::RefieUpdate>, FormRejection>,
) -> axum::response::Result<html::RefieView, (StatusCode, html::ErrorSet)>
{
    let form = match form {
        Ok(Form(data)) => data,
        Err(error)     => return Err((error.status(), html::ErrorSet::from(error))),
    };

    let refie = Refie::builder(id, form.label)
        .with_range(model::Time::from(form.from), model::Time::from(form.upto))
        .build();

    let mut guard = storage.lock().await;

    match guard.update(refie) {
        Ok(refie)  => Ok(html::RefieView::from(refie.clone())),
        Err(error) => Err((StatusCode::NOT_FOUND, html::ErrorSet::from(error))),
    }
}

// -------------------------------------------------------------------------------------------------
// DELETE /refie/:id

/// Delete [`Refie`]s by `id`
pub async fn remove(
    State(storage): State<Storage>,
    Path(id): Path<RefieId>,
) -> axum::response::Response
{
    let mut guard = storage.lock().await;
    guard.remove(id);

    Redirect::to("/refie").into_response()
}

// -------------------------------------------------------------------------------------------------
// GET /refie/:id

/// Display [`Refie`]s by `id` (read-only)
pub async fn view(
    State(storage): State<Storage>,
    Path(id): Path<RefieId>,
) -> axum::response::Response {
    let guard = storage.lock().await;
    match guard.get(id) {
        None => {
            StatusCode::NOT_FOUND.into_response()
        },
        Some(refie) => {
            html::RefieView::from(refie.clone()).into_response()
        },
    }
}

// -------------------------------------------------------------------------------------------------
// GET /refie/:id/edit

/// Display [`Refie`]s by `id` (read-write)
pub async fn edit(
    State(storage): State<Storage>,
    Path(id): Path<RefieId>,
) -> axum::response::Response {
    let guard = storage.lock().await;
    match guard.get(id) {
        None => {
            StatusCode::NOT_FOUND.into_response()
        },
        Some(refie) => {
            html::RefieEdit::from(refie.clone()).into_response()
        },
    }
}
