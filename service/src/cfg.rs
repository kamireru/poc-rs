// -------------------------------------------------------------------------------------------------
// WebKind

#[derive(Clone,Copy,Debug,PartialEq,Eq)]
pub enum WebKind {
    Askama,
    Leptos
}

impl WebKind {
    pub fn iter() -> WebIter { 
        WebIter::new(Self::Askama)
    }

    pub fn as_str(&self) -> &'static str {
        match self {
            Self::Askama => "askama",
            Self::Leptos => "leptos",
        }
    }
}

impl std::default::Default for WebKind {
    fn default() -> Self {
        Self::Askama
    }
}

impl std::fmt::Display for WebKind {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(self.as_str())
    }
}

impl std::str::FromStr for WebKind {
    type Err = std::io::Error;

    fn from_str(text: &str) -> Result<Self, Self::Err> {
        use std::io::Error;
        use std::io::ErrorKind;

        Self::iter()
            .find(|web| text.eq_ignore_ascii_case(web.as_str()))
            .ok_or_else(|| Error::new(
                ErrorKind::InvalidInput,
                format!("unknown web backend variant '{}'", text)
            ))
    }
}

// -------------------------------------------------------------------------------------------------
// WebIter

#[derive(Clone,Copy,Debug,PartialEq,Eq)]
pub struct WebIter {

    next: Option<WebKind>,
}

impl WebIter {
    pub fn new(from: WebKind) -> Self {
        Self {
            next: Some(from),
        }
    }
}

impl std::iter::Iterator for WebIter {
    type Item = WebKind;

    fn next(&mut self) -> Option<Self::Item> {
        match self.next {
            Some(WebKind::Askama) => self.next.replace(WebKind::Leptos),
            Some(WebKind::Leptos) => self.next.take(),
            None => None,
        }
    }
}
