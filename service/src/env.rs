use std::net::SocketAddr;
use std::path::PathBuf;
use std::time::Duration;

use anyhow::Context;

use crate::cfg::WebKind;


// -------------------------------------------------------------------------------------------------
// variables

const LISTEN: &str = "SERVICE_LISTEN";

const WEB_ASSETS:  &str = "SERVICE_WEB_ASSETS";
const WEB_BACKEND: &str = "SERVICE_WEB_BACKEND";

const STORE_LOAD:  &str = "SERVICE_STORE_LOAD";
const STORE_DELAY: &str = "SERVICE_STORE_DELAY";

// -------------------------------------------------------------------------------------------------
// listen()

pub fn listen() -> anyhow::Result<SocketAddr> {
    std::env::var(LISTEN)
        .unwrap_or_else(|_| String::from("127.0.0.1:3000"))
        .parse::<SocketAddr>()
        .with_context(|| format!("failed to parse {}", LISTEN))
}

// -------------------------------------------------------------------------------------------------
// web_assets()

pub fn web_assets() -> anyhow::Result<PathBuf> {
    let assets = match std::env::var(WEB_ASSETS) {
        Ok(v)  => PathBuf::from(v),
        Err(_) => PathBuf::from("assets"),
    };

    if assets.is_dir() {
        Ok(assets)
    }
    else {
        Err(anyhow::Error::msg(format!("{:?}: file not found", assets)))
    }
}

// -------------------------------------------------------------------------------------------------
// web_backend()

pub fn web_backend() -> anyhow::Result<WebKind> {
    match std::env::var(WEB_BACKEND) {
        Ok(text) => {
            text.parse::<WebKind>()
                .with_context(|| format!("failed to parse {}", WEB_BACKEND))
        },
        Err(_) => {
            Ok(WebKind::default())
        }
    }
}

// -------------------------------------------------------------------------------------------------
// store_load()

pub fn store_load() -> Option<PathBuf> {
    std::env::var(STORE_LOAD)
        .map(PathBuf::from)
        .ok()
}

// -------------------------------------------------------------------------------------------------
// store_delay()

pub fn store_delay() -> anyhow::Result<Duration> {
    std::env::var(STORE_DELAY)
        .unwrap_or_else(|_| String::from("0"))
        .parse::<u64>()
        .map(Duration::from_millis)
        .with_context(|| format!("failed to parse {}", STORE_DELAY))
}

