use anyhow::anyhow;

mod cfg;
mod env;
mod store;

#[tokio::main(flavor = "current_thread")]
async fn main() -> anyhow::Result<()> {
    init_trace();
    init_env();

    let listen      = env::listen()?;
    let web_assets  = env::web_assets()?;
    let web_backend = env::web_backend()?;
    let store_load  = env::store_load();
    let store_delay = env::store_delay()?;

    tracing::info!("set listen = {}", listen);
    tracing::info!("set web.backend = {}", web_backend);
    tracing::info!("set web.assets  = {:?}", web_assets);
    tracing::info!("set store.load  = {:?}", store_load);
    tracing::info!("set store.delay = {:?}", store_delay);

    let mut storage = match store_load {
        Some(path) => store::load(&path)?,
        None       => store::init(),
    };

    storage = storage
        .per_item_wait(store_delay);

    let server = web_server::Server::new(storage)
        .assets(web_assets)
        .routes(match web_backend {
            #[cfg(feature="askama")]
            cfg::WebKind::Askama => web_askama::routes(),

            _ => return Err(anyhow!("{}: web backend not implemented", web_backend)),
        });

    server
        .serve_at(listen).await
        .map_err(Into::into)
}

// -------------------------------------------------------------------------------------------------
// init_trace()

fn init_trace() {
    use tracing_subscriber::filter::EnvFilter;
    use tracing_subscriber::filter::LevelFilter;

    let filter = EnvFilter::builder()
        .with_default_directive(LevelFilter::INFO.into())
        .with_env_var("SERVICE_LOG")
        .from_env_lossy();

    tracing_subscriber::fmt()
        .with_target(false)
        .with_env_filter(filter)
        .init()
}

// -------------------------------------------------------------------------------------------------
// init_env()

fn init_env() {
    if dotenvy::dotenv().is_err() {
        tracing::warn!("ignore absent .env file");
    }
}
