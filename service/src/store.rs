use std::path::Path;
use std::fs::File;

use anyhow::Context;

use storage::Storage;
use storage::model::Refie;
use storage::model::NoId;

// -------------------------------------------------------------------------------------------------
// init()

#[inline]
pub fn init() -> Storage {
    Storage::default()
}

// -------------------------------------------------------------------------------------------------
// load()

pub fn load(path: &Path) -> anyhow::Result<Storage> {
    let file = File::open(path)
        .with_context(|| format!("failed to open {:?}", path))?;

    let list: Vec<Refie<NoId>> = serde_json::from_reader(file)
        .with_context(|| format!("failed to parse {:?}", path))?;

    Ok(Storage::from(list))
}
