Service Binary
================================================================================

This file stores usage and configuration details for `poc` service binary.


Configuration
--------------------------------------------------------------------------------

The service binary reads config from process ENV like most microservices. In
addition, it provides support for `.env` file loading.

| Variable            | Default        | Description                         |
| -                   | -              | -                                   |
| SERVICE_LISTEN      | 127.0.0.1:3000 | IP addr and port to bind to         |
| SERVICE_WEB_ASSETS  | "assets"       | path to static asset directory      |
| SERVICE_STORE_LOAD  | -              | path to JSON file with initial data |
| SERVICE_STORE_DELAY | -              | delay for each item in store (ms)   |


### SERVICE_STORE_DELAY

Variable configures number of milliseconds that each data store access will
take per-stored item. Which means that storage will grow slower with number of
items in it.

The aim is to introduce artifical slowness to backend so that we can see
loading indicators on frontend actually show.
