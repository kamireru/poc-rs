use axum::response::IntoResponse;

// -------------------------------------------------------------------------------------------------
// GET /favicon.ico

pub async fn favicon() -> impl IntoResponse {
    ().into_response()
}
