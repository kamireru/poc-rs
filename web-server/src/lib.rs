use std::net::SocketAddr;
use std::path::PathBuf;
use std::time::Duration;

use tracing::Span;

use axum::Router;
use axum::http::Response;
use axum::http::Request;
use axum::routing::get;

use tower_http::services::ServeDir;
use tower_http::trace::TraceLayer;

use storage::Storage;

mod route;


// -------------------------------------------------------------------------------------------------
// Server

#[must_use]
#[derive(Clone,Debug)]
pub struct Server {
    store: Storage,
    assets: Option<PathBuf>,
    routes: Option<Router<Storage>>,
}

impl Server {

    pub fn new(store: Storage) -> Self {
        Self {
            store,
            assets: None,
            routes: None,
        }
    }
    
    /// Set directory from which to serve assets
    pub fn assets<T: Into<PathBuf>>(mut self, assets: T) -> Self {
        self.assets = Some(assets.into());
        self
    }

    /// Set [`axum::Router`] for handling requests
    pub fn routes(mut self, routes: Router<Storage>) -> Self {
        self.routes = Some(routes);
        self
    }

    pub async fn serve_at(mut self, socket: SocketAddr) -> Result<(), std::io::Error> {
        let app    = self.build_app().with_state(self.store);
        let listen = tokio::net::TcpListener::bind(socket).await?;

        axum::serve(listen, app).await
    }
    
    fn build_app(&mut self) -> Router<Storage> {
        let router = self.make_router();

        let builder = tower::ServiceBuilder::new()
            .layer(
                TraceLayer::new_for_http()
                    .make_span_with(|request: &Request<_>| {
                        tracing::info_span!(
                            "web",
                            method = %request.method(),
                            path   = %request.uri().path(),
                        )
                    })
                    .on_response(|response: &Response<_>, latency: Duration, span: &Span| {
                        let status = response.status().as_u16();

                        span.record("status_code", tracing::field::display(status));
                        span.record("latency", tracing::field::debug(latency));

                        tracing::info!(%status, ?latency)
                    })
            );

        router.layer(builder)
    }

    fn make_router(&mut self) -> Router<Storage> {
        let mut router = Router::new()
            .route("/favicon.ico", get(route::favicon));

        if let Some(dir) = self.assets.take() {
            let serve_dir = ServeDir::new(dir)
                .precompressed_gzip();
            router = router.nest_service("/assets", serve_dir);
        }

        if let Some(routes) = self.routes.take() {
            router = router.nest("/", routes);
        }

        router
    }
}
