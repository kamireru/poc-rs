README
================================================================================

Repository hosts proof-of-concept project for combining JavaScript htmx library
with Rust based server-side rendered web service.


Motivation
--------------------------------------------------------------------------------

Getting into full-frontend development using either JavaScript or Rust
libraries is major issue for me. Have tried multiple times, failed.

This time I'm trying to use htmx library to *avoid* full frontend development.

Instead render everything on server side and handle partial DOM updates using
htmx library hooks.


Technology
--------------------------------------------------------------------------------

### Client Side

The client side uses only one library and the whole evaluation is done using it:

- [htmx](https://htmx.org) (JavaScript)


### Server Side

The server side is bit more complex. It uses following libraries as backbone, a
proven solutions:

- [axum](https://docs.rs/axum/latest/axum/)

In addition, it explores several ways to do server side rendering and how it
works in combination with `axum` and `htmx`:

- [askama](https://docs.rs/askama/latest/askama)


Roadmap
--------------------------------------------------------------------------------

### Frontend

- [x] use htmx for easy SSR
- [ ] form validation
- [ ] open time range handling
  - compute time upto from current time
  - compute time span from current time

### Backend

- [ ] support open range (start, no duration)
- [ ] support floating range (no start, only duration
- [x] sane parse/validation error handling
  - [x] communicate error on parsing
  - [ ] communicate error on model validation
  - [ ] return user-readable errors
- [x] time representation
  - [x] use time for start
  - [x] use span for duration

### Storage

- [x] management (list, add, remove, update)
- [x] storage using database-like row
  - [x] create row without id
  - [x] update row with id on insert (auto-generated)
- [ ] use 
