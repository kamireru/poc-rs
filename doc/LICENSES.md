3rd Party Code Licenses
================================================================================

htmx
--------------------------------------------------------------------------------

Source: https://github.com/bigskysoftware/htmx
License: Zero-Clause BSD
Files:
- ../assets/js/htmx@2.0.1.js.gz

htmx-ext-response-target
--------------------------------------------------------------------------------

Source: https://github.com/bigskysoftware/htmx-extensions
License: Zero-Clause BSD
Files:
- ../assets/js/htmx-ext-response-targets@2.0.0.js.gz
