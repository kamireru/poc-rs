# Summary

- [Overview](./overview.md)
- [Frontend]()
  - [HTMX](./frontend/htmx.md)
- [Backend]()
  - [Askama](./backend/askama.md)
- [Design](./design/overview.md)
  - [Backend vs Storage Serde](./design/serde_backend_vs_storage.md)
- [Patterns]()
  - [`trait` IdType<T>](pattern/id_type_trait.md)
