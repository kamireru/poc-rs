IdType
================================================================================

A trait that marks a type used for database identifiers. Allows using same
struct to represent both persistet record (with ID) and to-be persisted record
(without id).

Resources
--------------------------------------------------------------------------------

- [Andreas Fuch: Some useful types for database-using Rust web apps](
  https://boinkor.net/2024/04/some-useful-types-for-d
