Askama Notes
================================================================================

<!-- toc -->

Issues
--------------------------------------------------------------------------------

### Template Struct Methods

Askama provides way to run function of the template struct itself. But it
forces us to use *function call notation*:

```
{{ Self::func(self, args) }}
```

Which is quite verbose and kinda defeats the purpose of such methods - to
simplify and shorten the code using helpers.

In some cases we can work-around the issue by using struct fields and
pre-computing stuff in constructor.
