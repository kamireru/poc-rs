Serde in Backend vs Storage
================================================================================

The application is very limited in scope and there are places where we *can*
cut corners and re-use same data types on multiple layers. And if it was real
application, it would make sense to do so.

But this is a prototype/proof-of-concept app. Therefore we are doing the design
as-if it was really complex app that needs different layers to prevent mess.

In this chapter we will be noting what those design choices might be.
