Serde in Backend vs Storage
================================================================================

There are several data types that are needed on all layer, starting with
frontend and ending in storage.

In real world application we would have different data type for all of them:

1. need to be show in frontend
2. need to be parsed by backend
3. need to be persisted by storage

In limited app, we could reuse same types on all layers, we just have to make
sure that their (de)serialization work on all of them. 

In proof-of-concept, we focus on differences between (1) and (2). We are
reusing backend entity for (3) too.


Time and Span
--------------------------------------------------------------------------------

### Frontend

We are using single `Time` abstraction. It is good enough because
frontend deals mainly with display. We do not have to be too anal about having
different concept desribed by different type.

### Backend/Storage

We have separate types for time offset and duration, because we *have*
worry about accidental mixing of those two.

Because we are mixing (2) and (3), we have to support (de)serialization here.
But we are using simple and limited variant. Enough for persistence, not enough
for human interaction.
