HTMX
================================================================================


HTTP Error Status Codes
--------------------------------------------------------------------------------


But we would like to do error display/handling the same way we do success
response display - plop response content to some DOM element.

But the default handling in `htmx` is to fail on any `4xx` and `5xx` status
code, log to console and do absolutely nothing.

It is possible to handle it:

- extend HTMX internals (blegh, javascript)
- use `response-targets` extension


Resources
--------------------------------------------------------------------------------

- [htmx-ext: response-targets](
  https://github.com/bigskysoftware/htmx-extensions/blob/main/src/response-targets/README.md)
